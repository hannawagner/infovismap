## InfoVis Project

by Matej Rajtár and Hanna Wagner


## About

The general idea is to show the differences between abstract metro maps 
from various cities and their true geometry/geographical coordinates. 

This visualisation in particular shows the comparison for the city Vienna.


## Start

`python3 -m http.server`

`http://localhost:8000/home.html`